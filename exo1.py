import networkx as nx 
import matplotlib.pyplot as plt
import numpy as np

# Question n°1 : Création des graphes orienté et non orienté
G = nx.Graph()   # graphe non orienté G
DG = nx.DiGraph() # graphe orienté DG

# Question n°2 : Ajout des sommets à ces deux graphes
G.add_nodes_from([1,2,3,4,5])
DG.add_nodes_from([1,2,3,4,5])

# Question n°3 : Afichage des sommets ajoutés 
#  nx.draw(G)
#  nx.draw(DG)
#  plt.show()

# Question n°4 : Suppression du sommet 1 du graph G
G.remove_node(1)


# Question n°5 : Ajout des arêtes à G
G.add_edge(2,3)
G.add_edge(2,5)
G.add_edge(3,4)
G.add_edge(4,5)



# Question n°6 : Ajout des arêtes à DG
DG.add_edge(1,3)
DG.add_edge(2,5)
DG.add_edge(2,4)
DG.add_edge(2,5)
DG.add_edge(4,5)
DG.add_edge(5,1)


# Question n°7 : Tracer les deux graphes 
nx.draw(G)
nx.draw(DG)
plt.show()