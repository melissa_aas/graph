import networkx as nx 
import matplotlib.pyplot as plt


G = nx.Graph()
G.add_nodes_from(range(1, 10))

G.add_weighted_edges_from([
    (1, 2, 2), (2, 3, 2), (3, 1, 1), (3, 5, 5), (2, 4, 1),
    (2, 7, 3), (4, 5, 3), (4, 6, 3), (5, 6, 1), (6, 7, 4),
    (6, 8, 1), (6, 9, 3), (7, 8, 5), (8, 9, 2)
])

pos = {
    1: (0.5, 0.5),       
    2: (1, 0.7),       
    3: (1, 0.3),   
    4: (1.5, 0.5),   
    5: (1.5, 0.2),    
    6: (2, 0.3),    
    7: (2.5, 0.7),       
    8: (2.5, 0.3),   
    9: (2.5, 0),    
}


nx.draw(G, pos, with_labels=True, node_color='lightblue', font_weight='bold', node_size=700, width=2)

edge_weights = nx.get_edge_attributes(G, 'weight')
nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_weights)

#plt.show()

def test_non_isole(G):
    degrees = G.degree()
    for node, degree in degrees:
        if degree == 0:
            return False
    return True

# Exemple d'utilisation
print(test_non_isole(G))  # Renvoie True


def AlgoACM(G):
    if not nx.is_connected(G):
        raise ValueError("Le graphe n'est pas connexe.")
    
    acm = nx.minimum_spanning_tree(G)
    weight = sum(weight for _, _, weight in acm.edges(data='weight'))
    
    return acm, weight

# Exemple d'utilisation
acm, weight = AlgoACM(G)
print(f"Poids de l'arbre couvrant minimal : {weight}")

acm, weight = AlgoACM(G)

pos_acm = {1: (0, 0), 2: (1, 0), 3: (2, 0), 4: (0, 1), 5: (1, 1), 6: (2, 1), 7: (0, 2), 8: (1, 2), 9: (2, 2)}

nx.draw_networkx_nodes(G, pos_acm, node_size=700, node_color='pink')
nx.draw_networkx_labels(G, pos_acm, labels={node: str(node) for node in G.nodes}, font_size=12, font_color='black')
nx.draw_networkx_edges(G, pos_acm, width=1)

nx.draw_networkx_edges(acm, pos_acm, width=2, edge_color='red')

plt.axis('off')
plt.show()
