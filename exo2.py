import networkx as nx 
import matplotlib.pyplot as plt
import numpy as np

G = nx.Graph()
G.add_nodes_from([1,2,3,4,5,6,7,8,9])
G.add_edge(1,2)
G.add_edge(2,3)
G.add_edge(3,6)
G.add_edge(4,5)
G.add_edge(1,4)
G.add_edge(2,5)
G.add_edge(7,8)
G.add_edge(8,9)
G.add_edge(4,7)
G.add_edge(5,8)
G.add_edge(6,9)
G.add_edge(5,6)

positions = {1: (0, 0), 
             2: (1, 0), 
             3: (2, 0), 
             4: (0, 1), 
             5: (1, 1), 
             6: (2, 1), 
             7: (0, 2), 
             8: (1, 2), 
             9: (2, 2)
            }
#nx.draw(G,positions)
#plt.show()

G2= nx.Graph()
G2.add_nodes_from([1,2,3,4,5,6])
G2.add_edge(1, 2)
G2.add_edge(1, 3)
G2.add_edge(2, 3)
G2.add_edge(4, 5)
G2.add_edge(4, 6)
G2.add_edge(6, 5)
nx.draw(G2)
plt.show()

