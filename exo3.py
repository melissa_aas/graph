import networkx as nx
import random
import matplotlib.pyplot as plt

def generate_random_graph():
    # Générer un nombre aléatoire de sommets entre 2 et 15
    num_nodes = random.randint(2, 15)

    # Créer un graphe non orienté
    G = nx.Graph()

    # Ajouter des sommets au graphe
    G.add_nodes_from(range(1, num_nodes + 1))

    # Ajouter des arêtes aléatoires entre les sommets existants
    for i in range(1, num_nodes + 1):
        for j in range(i + 1, num_nodes + 1):
            # L'existence de l'arête est aléatoire
            if random.choice([True, False]):
                G.add_edge(i, j)

    return G

random_graph = generate_random_graph()


nx.draw(random_graph, with_labels=True, font_weight='bold', node_color='skyblue', edge_color='gray')
plt.show()
